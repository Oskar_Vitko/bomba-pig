﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombSpawn : MonoBehaviour
{
    public static GameObject around;
    [SerializeField] private GameObject bombPrefab;

    private List<Vector3> noEmptyTargets = new List<Vector3>();
    private Vector3 target;

    private void Awake()
    {
        around = gameObject;
    }

    private bool CanSpawn()
    {
        foreach (var pos in noEmptyTargets)
        {
            if (target == pos)
            {
                return false;
            }
        }
        return true;
    }

    public void Spawn()
    {
        if (around.transform.childCount == 2 || around.transform.childCount == 4)
        {
            float x = 0, y = 0;
            for (int i = 0; i < around.transform.childCount; i++)
            {
                x += around.transform.GetChild(i).position.x;
                y += around.transform.GetChild(i).position.y;
            }
            target = new Vector3(x / around.transform.childCount, y / around.transform.childCount);
            if (CanSpawn())
            {
                Instantiate(bombPrefab, target, Quaternion.identity);
                noEmptyTargets.Add(target);
            }
        }
    }
}
