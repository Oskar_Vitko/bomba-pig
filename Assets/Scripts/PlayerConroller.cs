﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerConroller : MonoBehaviour
{
    public static GameObject player;

    [SerializeField] private Joystick joystick;

    private Rigidbody2D rb;

    [SerializeField] private float speed = 1f;
    [SerializeField] private float verticalOffset = 0.3f;
    private float horizontal;
    private float vertical;
    private float moveBorder = 0.5f;
    private float sideBorder = 0.4f;

    private void Awake()
    {
        player = gameObject;
    }

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        GetInput();
        Move();
    }

    private void GetInput()
    {
        horizontal = joystick.Horizontal;
        vertical = joystick.Vertical;
    }

    private void Move()
    {
        if (vertical > moveBorder && horizontal < sideBorder && horizontal > -sideBorder)
        {
            rb.velocity = new Vector2(verticalOffset, speed * vertical);
        }
        else if (vertical < -moveBorder && horizontal < sideBorder && horizontal > -sideBorder)
        {
            rb.velocity = new Vector2(-verticalOffset, speed * vertical);
        }
        else if (horizontal > moveBorder && vertical < sideBorder && vertical > -sideBorder ||
            horizontal < -moveBorder && vertical < sideBorder && vertical > -sideBorder)
        {
            rb.velocity = new Vector2(speed * horizontal, 0f);
        }
        else
        {
            rb.velocity = new Vector2(0f, 0f);
        }
    }
}
