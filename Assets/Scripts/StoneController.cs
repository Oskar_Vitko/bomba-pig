﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoneController : MonoBehaviour
{
    [SerializeField] private GameObject stones;
    private float offset = 0.4f;
    private SpriteRenderer stoneSprite;   
    private void Start()
    {
        stoneSprite = GetComponent<SpriteRenderer>();
    }

    private void Update()
    {
        if(transform.position.y + offset < PlayerConroller.player.transform.position.y)
        {
            stoneSprite.sortingOrder = -1;
        }
        else
        {
            stoneSprite.sortingOrder = -3;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject == PlayerConroller.player)
        {
            transform.parent = BombSpawn.around.transform;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject == PlayerConroller.player)
        {
            transform.parent = stones.transform;
        }
    }
}
